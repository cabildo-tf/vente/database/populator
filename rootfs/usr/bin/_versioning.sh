#!/bin/bash

if [ "${VOLUME_NAME}" = "" ]
then
    echo "Please provide volume name"
    exit 1
fi

if [ "${SERVICE_NAME}" = "" ]
then
    echo "Please provide service name"
    exit 1
fi

function exists_volume {
	echo "$(docker volume list  --format "table {{ .Name }}" | grep ${1})"
}

function clone_volume {
	echo "Creating destination volume \"$2\"..."
	docker volume create --name $2

	echo "Copying data from source volume \"$1\" to destination volume \"$2\"..."
	docker run --rm -v $1:/from -v $2:/to \
           alpine ash -c "cd /from ; cp -a . /to 2>/dev/null"
}

function get_last_version {
	filter="${1}-v"
	version=$(docker volume list  --format "table {{ .Name }}" | grep ${filter} | sed -E "s/${filter}(\d+)/\1/" | sed 's/^0*//' | tail -n 1)
	last_version=$(($version + 1))
	echo "$(printf "$1-v%03d" ${last_version})"
}

function remove_containers {
	containers=$(docker container list --all --format "{{ .Names }}" | grep $1)
	if [ ! -z "${containers}" ]
	then
		if echo "${containers}" | xargs docker container rm
		then
			echo "Delete containers of service $1"
		else
			exit 1
		fi
	else
		echo "There aren't containers for ${1} service"
	fi
}

echo "Stopping service ${SERVICE_NAME}"
if docker service scale ${SERVICE_NAME}=0
then
	echo "Service \"${SERVICE_NAME}\" stopped"
else
	exit 1
fi

volume_name_version=$(get_last_version ${VOLUME_NAME})
echo "Last version: ${volume_name_version}"

echo "Versioning volume \"${VOLUME_NAME}\""
clone_volume ${VOLUME_NAME} ${volume_name_version}

echo "Removing containers of service \"${SERVICE_NAME}\""
remove_containers ${SERVICE_NAME}

echo "Removing volume of service \"${SERVICE_NAME}\""
if docker volume rm ${VOLUME_NAME}
then
	echo "Volume \"${VOLUME_NAME}\" deleted"
else
	exit 1
fi

echo "Starting service ${SERVICE_NAME}"
if docker service scale ${SERVICE_NAME}=1
then
	echo "Service \"${SERVICE_NAME}\" started"
else
	exit 1
fi
