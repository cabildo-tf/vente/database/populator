ARG DOCKER_VERSION=20.10.10-alpine3.14

FROM docker:${DOCKER_VERSION}

ENV PGPASSFILE='/root/.pgpass' \
	POSTGRES_SDE_USERNAME='sde' \
	BACKUPS_FOLDER='/backups' \
	PORT_DB="5432"

ARG POSTGRES_CLIENT_VERSION="11"

RUN apk add --no-cache \
		postgresql-client=~${POSTGRES_CLIENT_VERSION} \
			--repository=http://dl-cdn.alpinelinux.org/alpine/v3.10/main && \
    apk add --no-cache \
        bash=~5

COPY rootfs /

ENTRYPOINT ["/usr/bin/populator.sh"]