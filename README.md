# Populator
Restaura la última copia de seguridad de la base de datos, para ello sigue los siguientes pasos:
* Para el servicio de la base de datos
* Clona el volumen de datos
* Arranca el servicio de base de datos
* Restaura la copia de seguridad

# ArcGIS Geodatabase
Una vez restaurada la base de datos, es necesario para el servicio de ArcGIS Server y cerrar todas las conexiones a la base de datos.

```sql
SELECT *, pg_terminate_backend(pid)
FROM pg_stat_activity 
WHERE pid <> pg_backend_pid()
AND datname = 'vente';
```

Desde el ArcGIS Pro, usando la herramienta Data Management Tools -> Upgrade Geodatabase, realizar un upgrade de la base de datos, es necesario utilizar la conexión con el usuario SDE para realizar esta operación.